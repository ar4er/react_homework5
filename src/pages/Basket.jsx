import ProductList from "../component/product-list/ProductList";
import BuyForm from "../component/form/buy-form/BuyForm";
import styles from "../styles/index.module.scss";
import { useSelector } from "react-redux";

export function Basket() {
  const products = useSelector((state) => state.products.productsInBasket);
  return products.length ? (
    <div className={styles.Basket}>
      <ProductList page="basket" products={products} />
      <BuyForm />
    </div>
  ) : (
    "Корзина порожня ):"
  );
}
