import { magazineTypes } from "../types";

export function openModal(modalId, submitFunction, data) {
  return {
    type: magazineTypes.OPEN_MODAL,
    payload: {
      modalId,
      submitFunction,
      data,
    },
  };
}

export function closeModal() {
  return {
    type: magazineTypes.CLOSE_MODAL,
  };
}
