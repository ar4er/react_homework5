import { magazineTypes } from "../types";

function getAllProducts(products) {
  return {
    type: magazineTypes.GET_ALL_PRODUCTS,
    payload: {
      products,
    },
  };
}

export function fetchProductsArray() {
  return async function (dispatch) {
    const data = await fetch("productCollection.json").then((response) => {
      return response.json();
    });
    dispatch(getAllProducts(data));
  };
}

export function addProductToFavorite(product) {
  return {
    type: magazineTypes.ADD_PRODUCT_TO_FAVORITE,
    payload: {
      product,
    },
  };
}

export function removeProductFromFavorite(article) {
  return {
    type: magazineTypes.REMOVE_PRODUCT_FROM_FAVORITE,
    payload: {
      article,
    },
  };
}

export function addProductToBasket(product) {
  return {
    type: magazineTypes.ADD_PRODUCT_TO_BASKET,
    payload: {
      product,
    },
  };
}

export function removeProductFromBasket(article) {
  return {
    type: magazineTypes.REMOVE_PRODUCT_FROM_BASKET,
    payload: {
      article,
    },
  };
}

export function clearBasket() {
  return {
    type: magazineTypes.CLEAR_BASKET,
  };
}
