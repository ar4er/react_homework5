import { magazineTypes } from "../types";

const initialState = {
  visible: false,
  modalId: "default",
  submitFunction: null,
  data: null,
};

export function modalReducer(state = initialState, action) {
  switch (action.type) {
    case magazineTypes.OPEN_MODAL:
      return {
        ...state,
        visible: true,
        modalId: action.payload.modalId,
        submitFunction: action.payload.submitFunction,
        data: action.payload.data,
      };
    case magazineTypes.CLOSE_MODAL:
      return {
        ...state,
        visible: false,
        modalId: "default",
        submitFunction: null,
        data: null,
      };
    default:
      return state;
  }
}
