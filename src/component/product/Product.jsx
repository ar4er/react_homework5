import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { openModal } from "../../redux/actions/modal";
import {
  addProductToBasket,
  addProductToFavorite,
  removeProductFromFavorite,
  removeProductFromBasket,
} from "../../redux/actions/products";
import PropTypes from "prop-types";
import Button from "../UI/button/Button";
import styles from "./Product.module.scss";
import favoriteStar from "../../image/favourites-star.png";
import favoriteStarBlack from "../../image/favourites-star-black.png";

function Product({ page, ...props }) {
  const [inBasket, setInBasket] = useState(false);
  const [inFavorite, setInFavorite] = useState(false);

  const productsInFavorite = useSelector(
    (state) => state.products.productsInFavorite
  );
  const productsInBasket = useSelector(
    (state) => state.products.productsInBasket
  );
  const dispatch = useDispatch();

  useEffect(() => {
    const productInFavorite = [...productsInFavorite].find(
      (item) => item.article === props.product.article
    );
    productInFavorite && setInFavorite(true);

    const productInBasket = [...productsInBasket].find(
      (product) => product.article === props.product.article
    );
    productInBasket && setInBasket(true);
  }, [props.product, productsInBasket, productsInFavorite]);

  const addToFavorite = () => {
    dispatch(addProductToFavorite(props.product));
    setInFavorite(true);
  };

  const removeFromFavorite = () => {
    dispatch(removeProductFromFavorite(props.product.article));
    setInFavorite(false);
  };

  const addToBasket = () => {
    dispatch(addProductToBasket(props.product));
    setInBasket(true);
  };

  const removeFromBasket = () => {
    dispatch(removeProductFromBasket(props.product.article));
    setInBasket(false);
  };

  const { title, price, imgUrl, color } = props.product;

  return (
    <div className={page === "basket" ? styles.Basket : styles.Product}>
      <div
        className={styles.Favorite}
        onClick={() => {
          inFavorite ? removeFromFavorite() : addToFavorite();
        }}
      >
        <img src={inFavorite ? favoriteStarBlack : favoriteStar} alt="" />
      </div>
      <div className={styles.imageWrapper}>
        <img src={imgUrl} alt="" />
      </div>
      <div className={styles.Info}>
        <h2>{title}</h2>
        <h4>Колір: {color}</h4>
        <h3>Ціна: {price} грн</h3>
      </div>
      <Button
        text={
          (page === "basket" && "Видалити?") ||
          (inBasket ? "Вже в кошику" : "До кошика")
        }
        backgroundColor={inBasket ? "grey" : "yellow"}
        onClick={() => {
          (page === "home" || page === "favorite") && !inBasket
            ? dispatch(
                openModal("addToBasket", addToBasket, props.product.title)
              )
            : page === "basket" &&
              dispatch(
                openModal(
                  "removeFromBasket",
                  removeFromBasket,
                  props.product.title
                )
              );
        }}
      />
    </div>
  );
}

Product.propTypes = {
  product: PropTypes.object,
  page: PropTypes.string,
};

export default Product;
