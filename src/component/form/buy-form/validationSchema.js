import * as Yup from "yup";

const phoneNumberValidRegExp =
  /^(\+38)[ ](\(0[0-9]{2}\)[ ][0-9]{3}[ ][0-9]{2}[ ][0-9]{2})$/gm;

export const validationSchema = Yup.object({
  firstName: Yup.string()
    .min(3, "Мінімум 3 символи")
    .max(50, "Мксимум 50 символів")
    .required("Ім'я обов'язкове"),
  lastName: Yup.string()
    .min(3, "Мінімум 3 символи")
    .max(50, "Мксимум 50 символів")
    .required("Прізвище обов'язкове"),
  age: Yup.number()
    .min(18, "Мінімальний вік 18")
    .max(100, "Максимальний вік 100")
    .required("Вік обов'язковий"),
  adress: Yup.string()
    .min(3, "Мінімум 3 символи")
    .max(50, "Мксимум 50 символів")
    .required("Адреса обов'язкова"),
  phoneNumber: Yup.string()
    .matches(phoneNumberValidRegExp, "Введіть коректний номер")
    .required("Телефон обов'язковий"),
});
